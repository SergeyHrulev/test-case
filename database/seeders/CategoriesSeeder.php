<?php

namespace Database\Seeders;

use App\Models\Goods;
use App\Models\Categories;
use Database\Factories\CategoriesFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categories::factory()
            ->count(15)
            ->has(Goods::factory()->count(12), 'good')
            ->create();
    }
}
