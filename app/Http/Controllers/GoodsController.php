<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreGoodsRequest;
//use App\Http\Requests\UpdateGoodsRequest;
use App\Http\Resources\GoodsResource;
use App\Models\Categories;
use App\Models\Goods;
use Illuminate\Http\Request;

class GoodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreGoodsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGoodsRequest $request)
    {
        $categories = Categories::where('id', $request->categories_id)->withCount('good')->pluck('good_count')->first();
        if ($categories > 10) {
            return response('Количество товаров в категории не может превышать 10', 403);
        }
        $category = Categories::findOrFail($request->categories_id);
        return $category->good()->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Goods  $goods
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Goods $goods)
    {
        if ($request->has('name')) {
            return GoodsResource::collection($goods->searchByName($request->name));
        }
        if ($request->has('categories_id')) {
            return GoodsResource::collection($goods->searchByCategoryId($request->categories_id));
        }
        if ($request->has('category_name')) {
            return GoodsResource::collection($goods->searchByCategoryName($request->category_name));
        }
        if ($request->has('price_min') && $request->has('price_max')) {
            return GoodsResource::collection($goods->searchByMinMaxPrice($request->price_min, $request->price_max));
        }
        if ($request->has('published')) {
            return GoodsResource::collection($goods->searchByPublished());
        }
        if ($request->has('not_deleted')) {
          return GoodsResource::collection($goods->all());
        } else {
            return response('Ничего не найдено', 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Goods  $goods
     * @return \Illuminate\Http\Response
     */
    public function edit(Goods $goods)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateGoodsRequest  $request
     * @param  \App\Models\Goods  $goods
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categories = Categories::where('id', $request->categories_id)->withCount('good')->pluck('good_count')->first();
        if ($categories > 10) {
            return response('Количество товаров в категории не может превышать 10', 403);
        }

        $good = Goods::find($id);
        $good->fill($request->all());
        return $good->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Goods  $goods
     * @return \Illuminate\Http\Response
     */
    public function destroy(Goods $goods, $id)
    {
        return Goods::destroy($id);
    }
}
