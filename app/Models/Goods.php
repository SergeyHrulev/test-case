<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Goods extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [
        'id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function category()
    {
        return $this->belongsToMany(Categories::class, 'category_goods');
    }

    public function searchByName($name)
    {
        return $this->where('name', '=', $name)->get();
    }

    public function searchByCategoryId($categories_id)
    {
        return Goods::with('category')->where(function ($query) use ($categories_id){
            $query->whereHas('category', function ($q) use ($categories_id) {
                $q->where('id', $categories_id);
            });
        })->get();
    }

    public function searchByCategoryName($category_name)
    {
        return Goods::with('category')->where(function ($query) use ($category_name){
            $query->whereHas('category', function ($q) use ($category_name) {
                $q->where('name', $category_name);
            });
        })->get();
    }

    public function searchByMinMaxPrice($price_min, $price_max)
    {
        return $this->where('price', '>=', $price_min)
            ->where('price', '<=', $price_max)->get();
    }

    public function searchByPublished()
    {
        return $this->where('published', '=', true)->get();
    }

}
