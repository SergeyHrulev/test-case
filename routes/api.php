<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GoodsController;
use App\Http\Controllers\CategoriesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/goods/show/', [GoodsController::class, 'show']);

Route::post('/goods/{id}/edit', [GoodsController::class, 'update']);

Route::resource('goods', GoodsController::class)->only([
    'store', 'destroy'
]);
Route::resource('categories', CategoriesController::class)->only([
    'store', 'destroy'
]);
